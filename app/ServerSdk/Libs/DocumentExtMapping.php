<?php


namespace App\ServerSdk\Libs;


use Illuminate\Support\Arr;

class DocumentExtMapping
{
    public static array $mapping = [ // gldoc main <=> Document server
        1 => 15,
        2 => 1,
        3 => 2,
        4 => 5,
        41 => null,
        5 => 6,
        50 => null,
        51 => null,
        6 => 20,
        7 => null,
        8 => null,
    ];
    
    public static function ds2gldoc($ds_ext_id)
    {
        return array_search($ds_ext_id, self::$mapping) ?? $ds_ext_id;
    }
    
    public static function gldoc2ds($gldoc_ext_id)
    {
        return Arr::get(self::$mapping, $gldoc_ext_id, $gldoc_ext_id);
    }
    
    // pdf
//'pdf' => 1,
//    // office
//'doc' => 2,
//'docx' => 3,
//'ppt' => 4,
//'pot' => 41,
//'pptx' => 5,
//'ppsx' => 50,
//'pptm' => 51,
//    // open office
//'odt' => 6,
//    // text
//'txt' => 7,
//'md' => 8,
    
    // WORD
//    private const doc = 1;
//    private const docx = 2;
//    private const ppt = 5;
//    private const pptx = 6;
//    private const xls = 10;
//    private const xlsz = 11;
//
//    // PDF
//    private const pdf = 15;
//
//    // OPEN OFFICE
//    private const odt = 20;
}