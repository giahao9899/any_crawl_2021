<?php

namespace App\ServerSdk\Client\Server2Search;

use App\ServerSdk\Client\ClientAbstract;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

class Client extends ClientAbstract
{
    const uri_upload = 'api/document/upload';

    public function ping(): bool
    {
        // TODO: Implement ping() method.
    }

    /**
     * @throws GuzzleException|\App\ServerSdk\Exceptions\ServerBusyException
     */
    public function upload(array $data) {

        start_upload:
        try {
            $response = $this->request('POST', self::uri_upload, [
                'form_params' => $data,
            ]);

            $response_data = json_decode($response->getBody()->getContents(), true);

            if ($response_data['success']) {
                return [
                    'success' => true,
                    'document' => \Arr::only($response_data['document'],
                        [
                            'id',
                            'code',
                            'title',
                            'status',
                            'language',
                            'author',
                            'keywords',
                            'referer',
                            'download_link',
                        ])
                ];
            } else {
                dd($response_data);
            }
        } catch (RequestException|GuzzleException $ex) {
            dump("Request error :: " . $ex->getMessage());
            if ($ex->getResponse()) {
                $response_data = json_decode($ex->getResponse()->getBody()->getContents(), true);
                if ($response_data) {
                    $message = $response_data['message'] . implode(",", \Arr::get($response_data, 'errors.document', []));
                    return [
                        'success' => false,
                        'message' => $message
                    ];
                }
            } elseif (strpos($ex->getMessage(), "Connection timed out")) {
                goto start_upload;
            }
            throw $ex;
        }

    }
}