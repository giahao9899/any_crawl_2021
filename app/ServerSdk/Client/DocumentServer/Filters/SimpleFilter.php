<?php

namespace App\ServerSdk\Client\DocumentServer\Filters;

class SimpleFilter extends FilterAbstract
{
    public function id($id = null)
    {
        return $this->getSetRule('id', $id);
    }
    
    public function app_id($app_id = null)
    {
        return $this->getSetRule('app_id', $app_id);
    }
    
    public function code($code = null)
    {
        return $this->getSetRule('code', $code);
    }
    
    public function list_id($list_id = null)
    {
        return $this->getSetRule('list_id', $list_id);
    }
    
    public function range_id($range_id = null)
    {
        return $this->getSetRule('range_id', $range_id);
    }
    
    public function status($status = null)
    {
        return $this->getSetRule('status', $status);
    }
    
    /**
     * @param null $status 0|null|1
     *
     * @return SimpleFilter|mixed
     */
    public function pdf_status($status = null)
    {
        return $this->getSetRule('pdf_status', $status);
    }
}