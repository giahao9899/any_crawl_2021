<?php

namespace App\ServerSdk\Client\DocumentServer\Filters;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

abstract class FilterAbstract
{
    protected string $filter_prefix = 'filter_';
    protected string $order_prefix = 'order_';
    
    protected array $rules = [
        'limit' => 20,
        'page' => 1,
    ];
    
    public function toArray() {
        $data = [];
        foreach ($this->rules as $k => $v) {
            if (method_exists($this, $k)) {
                $data[$this->filter_prefix . $k] = $this->{$k}();
            } else {
                $data[$this->filter_prefix . $k] = $v;
            }
        }
        return $data;
    }
    
    public static function fromArray(array $request, $only = []) {
        $instance = new static();
        foreach ($request as $k => $v) {
            if (!Str::startsWith($k, $instance->filter_prefix)) {
                continue;
            }
            $k = Str::replaceFirst($instance->filter_prefix, "", $k);
            if (method_exists($instance, $k)
                && (count($only) == 0 || in_array($k, $only))) {
                $instance->{$k}($v);
            }
        }
        return $instance;
    }
    
    protected function getSetRule($key, $value = null) {
        if ($value === null) {
            return Arr::get($this->rules, $key);
        } else {
            $this->rules[$key] = $value;
            return $this;
        }
    }
    
    public function __toString() {
        return http_build_query($this->toArray());
    }
    
    public function limit($limit = null) {
        return $this->getSetRule('limit', $limit);
    }
    
    public function page($page = null) {
        return $this->getSetRule('page', $page);
    }
}