<?php


namespace App\ServerSdk\Client\DocumentServer;


use Carbon\Carbon;
use League\Uri\Components\Query;
use League\Uri\Uri;

class UrlHelper
{
    protected array $server_config = [
    
    ];
    
    protected string $document_code;
    
    /**
     * UrlHelper constructor.
     *
     * @param $server_name
     * @param $document_code
     *
     * @throws \Exception
     */
    public function __construct($server_name, $document_code) {
        $server_config = config('document_servers.servers.' . $server_name, false);
        if(!$server_config){
            throw new \Exception("No config for document server " . $server_name);
        }
        $this->server_config = [
            'name' => $server_name,
            'host' => rtrim( $server_config['host'], "/" ),
            'app' => $server_config['app'],
            'key' => $server_config['key'],
        ];
        $this->document_code = $document_code;
    }
    
    public function getPreviewLink(){
        return $this->server_config['host'] . "/viewer/" . $this->server_config['app'] . "/" . $this->document_code;
    }
    
    public function getRequestPreviewFileLink(){
        $url = $this->server_config['host'] . "/preview/" . $this->server_config['app'] . "/" . $this->document_code;
        return self::signUrl( $url, 600, $this->server_config['key']);
    }
    
    public function getDownloadLink($delay = 600){
        $url = $this->server_config['host'] . "/download/" . $this->server_config['app'] . "/" . $this->document_code;
        return self::signUrl( $url, $delay, $this->server_config['key']);
    }
    
    public static function hasValidSignature($url, $key)
    {
        $url = Uri::createFromString($url);
        $query = Query::createFromUri($url);
        $original_query = $query->withoutParam( 'signature');
        $expires = $query->get( 'expires');
        $original_url = $url->withQuery( $original_query->getContent() );
        $signature = hash_hmac('sha256', $original_url, $key);
        Carbon::createFromTimestamp( $expires, 'UTC');
        return hash_equals($signature, (string) $query->get('signature')) &&
            ! ($expires && Carbon::now('UTC')->getTimestamp() > $expires);
    }
    
    public static function signUrl($url, $delay, $key)
    {
        $url = Uri::createFromString($url);
        $query = Query::createFromUri($url);
        $original_query = $query->append( "expires=" . self::availableAt($delay));
        $original_url = $url->withQuery( $original_query->sort()->getContent() );
        $signature = hash_hmac('sha256', $original_url, $key);
        return $url->withQuery( $original_query->append( "signature=" . $signature)->sort()->getContent())->__toString();
    }
    
    /**
     * Get the "available at" UNIX timestamp.
     *
     * @param  \DateTimeInterface|\DateInterval|int  $delay
     * @return int
     */
    protected static function availableAt($delay = 0)
    {
        $delay = self::parseDateInterval($delay);
        
        return $delay instanceof \DateTimeInterface
            ? $delay->getTimestamp()
            : Carbon::now('UTC')->addRealSeconds($delay)->getTimestamp();
    }
    
    /**
     * If the given value is an interval, convert it to a DateTime instance.
     *
     * @param  \DateTimeInterface|\DateInterval|int  $delay
     * @return \DateTimeInterface|int
     */
    protected static function parseDateInterval($delay)
    {
        if ($delay instanceof \DateInterval) {
            $delay = Carbon::now()->add($delay);
        }
        
        return $delay;
    }
}