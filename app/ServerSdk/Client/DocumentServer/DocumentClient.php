<?php


namespace App\ServerSdk\Client\DocumentServer;


use App\ServerSdk\Client\DocumentServer\Entities\Document;
use App\ServerSdk\Client\DocumentServer\Entities\UploadingFile;
use App\ServerSdk\Client\DocumentServer\Entities\UploadResult;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Utils;

class DocumentClient extends DocumentServerClient
{
    const uri_upload = 'api/documents/upload';
    const uri_search = 'api/documents/search';
    const uri_xml = 'api/documents/xml';
    
    /**
     * @param UploadingFile $file
     *
     * @return UploadResult|void
     */
    public function upload(UploadingFile $file) {
        $data = $file->getMultipart();
    
        start_upload:
    
        try {
            $response = $this->client->post(self::uri_upload, [
                'multipart' => $data,
            ]);
    
            $response_data = json_decode($response->getBody()->getContents(), true);
    
            if ($response_data['success']) {
                return new UploadResult(Document::fromArray(
                    \Arr::only($response_data['document'],
                        [
                            'id',
                            'app_id',
                            'code',
                            'original_hash',
                            'status',
                            'language',
                            'referer',
                            'owner_id',
                        ])
                ), true);
            } else {
                dd($response_data);
            }
        
        } catch (RequestException $ex) {
            dump("Request error :: " . $ex->getMessage());
            if ($ex->getResponse()) {
                $response_data = json_decode($ex->getResponse()->getBody()->getContents(), true);
                if ($response_data) {
                    $message = $response_data['message'] . implode(",", \Arr::get($response_data, 'errors.document', []));
                    return new UploadResult(
                        Document::fromArray(\Arr::get($response_data, 'errors.existed_document.0', [])),
                        false,
                        $message
                    );
                }
            } elseif (strpos($ex->getMessage(), "Connection timed out")) {
                goto start_upload;
            }
            throw $ex;
        }
    }
    
    /**
     * @return UploadResult|void
     */
    public function uploadFromFile(string $path, string $title, string $keywords = '', string $source_url = '', array $additions = []) {
        $uploading_file = UploadingFile::fromFile($path, $title, $source_url, $keywords, $additions);
        return $this->upload($uploading_file);
    }
    
    /**
     * @return UploadResult|void
     */
    public function uploadFromContent($content, string $title, string $keywords = '', string $source_url = '', array $additions = []) {
        $uploading_file = UploadingFile::fromContent($content, $title, $source_url, $keywords, $additions);
        return $this->upload($uploading_file);
    }
    
    /**
     * @return UploadResult|void
     */
    public function uploadFromUrl(string $url, string $title, string $keywords = '', string $source_url = '', array $additions = []) {
        $uploading_file = UploadingFile::fromUrl($url, $title, $source_url, $keywords, $additions);
        return $this->upload($uploading_file);
    }
    
    /**
     * Tìm kiếm document theo từ khoá, và lọc theo filter
     * Filter supported fields
     * id, ids : danh sách hoặc dải id, status : trạng thái tài liệu
     *
     * @param string|null $term
     * @param array $filter
     *
     * @param int $page
     * @param int $limit
     *
     * @return array
     * @throws \App\ServerSdk\Exceptions\ServerBusyException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function search(?string $term = '', array $filter = [], int $page = 1, int $limit = 20): array
    {
        $query = ['page' => $page, 'limit' => $limit];
        if($term) {
            $query['search_term'] = $term;
        }
        $query = $query + $filter;
        
        $response = $this->request('GET', self::uri_search, [
            'query' => $query,
        ])->getBody()->getContents();
        
        $response = Utils::jsonDecode($response, true);
        
        return $response['data'];
    }
    
    public function getPdf(int $document_id)
    {
        //
    }
    
    /**
     * Get XML by Document_ID
     * @param int $document_id
     *
     * @return string|string[]
     * @throws \App\ServerSdk\Exceptions\ServerBusyException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getXml(int $document_id)
    {
        $uri = rtrim(self::uri_xml, '/') . '/' . $document_id;
        $response = $this->request('GET', $uri);
        
        $xml = $response->getBody()->getContents();
        return str_replace( " <page number=", "\n<page number=", $xml);
    }
}