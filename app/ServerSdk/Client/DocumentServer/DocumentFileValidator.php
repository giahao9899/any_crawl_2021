<?php

namespace App\ServerSdk\Client\DocumentServer;

use App\ServerSdk\Exceptions\InvalidDocumentException;
use GuzzleHttp\Client;

class DocumentFileValidator
{
    protected int $min_pages;
    protected int $min_words;
    protected int $min_characters;
    protected array $languages;
    protected array $extensions;
    
    /**
     * DocumentFileValidator constructor.
     *
     * @param int $min_pages
     * @param int $min_words
     * @param int $min_characters
     * @param array $languages
     * @param array $extensions
     */
    public function __construct($min_pages, $min_words, $min_characters, array $languages, array $extensions) {
        $this->min_pages = $min_pages;
        $this->min_words = $min_words;
        $this->min_characters = $min_characters;
        $this->languages = $languages;
        $this->extensions = $extensions;
    }
    
    /**
     * @param string $host
     *
     * @return DocumentFileValidator
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function createFromHost(string $host) {
        $url = rtrim($host, "/") . "/api/documents/validation_info";
        $response = (new Client([
            'verify' => false,
        ]))->get($url);
        $info = json_decode($response->getBody()->getContents(), true);
    
        return new self(
            $info['min_pages'],
            $info['min_words'],
            $info['min_characters'],
            $info['languages'],
            $info['extensions']
        );
    }
    
    /**
     * Check pages, content_characters, content_words, language, and original_ext
     *
     * @param array $data
     *
     * @throws InvalidDocumentException
     */
    public function check($data = []) {
        // pages
        if ($data['pages'] < $this->min_pages) {
            throw new InvalidDocumentException("Document too short, it have "
                . $data['pages']
                . " pages but system required "
                . $this->min_pages . " pages");
        }
    
        // content_characters
        if ($data['content_characters'] < $this->min_characters) {
            throw new InvalidDocumentException("Document too short, it have "
                . $data['content_characters']
                . " characters but system required "
                . $this->min_characters . " characters");
        }
    
        // content_words
        if ($data['content_words'] < $this->min_words) {
            throw new InvalidDocumentException("Document too short, it have "
                . $data['content_words']
                . " words but system required "
                . $this->min_words . " words");
        }
    
        // language
        if (!in_array($data['language'], $this->languages) && !in_array("*", $this->languages)) {
            throw new InvalidDocumentException("This document language (" . $data['language'] . ") is not valid, supported languages are " . implode(",", $this->languages));
        }
    
        //original_extension
        if (!is_numeric($data['original_ext'])) {
            if (!isset($this->extensions[$data['original_ext']])) {
                throw new InvalidDocumentException("This document extension (" . $data['original_ext'] . ") is not valid, supported languages are " . implode(",", array_flip($this->extensions)));
            }
            $data['original_ext'] = $this->extensions[$data['original_ext']];
        } elseif (!in_array($data['original_ext'], $this->extensions)) {
            throw new InvalidDocumentException("This document extension (" . $data['original_ext'] . ") is not valid, supported languages are " . implode(",", array_flip($this->extensions)));
        }
    
        return $data;
    }
}