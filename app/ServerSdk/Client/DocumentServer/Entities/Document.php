<?php


namespace App\ServerSdk\Client\DocumentServer\Entities;


use Illuminate\Support\Arr;

class Document
{
    public int $id;
    public int $app_id;
    public int $owner_id;
    public ?string $code;
    public string $language;
    public string $referer;
    public string $original_hash;
    // Urls
    public ?string $tmp_original;
    public ?string $tmp_pdf;
    public ?string $tmp_preview;
    public ?string $tmp_text;
    
    public array $additions = [];
    
    public static function fromArray($data = [])
    {
        $instance = new self();
        foreach ($data as $k => $v) {
            if (property_exists($instance, $k)) {
                $instance->{$k} = $v;
            } else {
                $instance->additions[$k] = $v;
            }
        }
        return $instance;
    }
    
    public function getInfo($key, $default = null)
    {
        if (property_exists($this, $key)) {
            return $this->{$key};
        } else {
            return Arr::get($this->additions, $key, $default);
        }
    }
}