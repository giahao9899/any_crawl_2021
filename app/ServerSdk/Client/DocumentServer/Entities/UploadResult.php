<?php


namespace App\ServerSdk\Client\DocumentServer\Entities;


class UploadResult
{
    protected bool $success = false;
    
    protected Document $document;
    protected string $message;
    
    /**
     * UploadResult constructor.
     *
     * @param Document $document
     * @param bool $success
     * @param string $message
     */
    public function __construct(Document $document, $success = true, $message = '') {
        $this->document = $document;
        $this->success = $success;
        $this->message = $message;
    }
    
    /**
     * @return bool
     */
    public function isSuccess(): bool {
        return $this->success;
    }
    
    /**
     * @param bool $success
     * @return UploadResult
     */
    public function setSuccess(bool $success): UploadResult {
        $this->success = $success;
        return $this;
    }
    
    /**
     * @return Document
     */
    public function getDocument(): Document {
        return $this->document;
    }
    
    /**
     * @param Document $document
     * @return UploadResult
     */
    public function setDocument(Document $document): UploadResult {
        $this->document = $document;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getMessage(): string {
        return $this->message;
    }
    
    /**
     * @param string $message
     * @return UploadResult
     */
    public function setMessage(string $message): UploadResult {
        $this->message = $message;
        return $this;
    }
}