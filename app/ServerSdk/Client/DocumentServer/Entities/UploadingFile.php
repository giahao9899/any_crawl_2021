<?php


namespace App\ServerSdk\Client\DocumentServer\Entities;

use App\ServerSdk\Exceptions\InvalidUploadingFileException;
use GuzzleHttp\Psr7\Utils;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class UploadingFile
{
    public string $title;
    public string $source_url;
    
    /** @var mixed */
    public $keywords;
    
    /** @var mixed */
    public $content;
    
    public array $additions = [];
    
    protected function __construct() {
        // use static functions
    }
    
    public function setAddition($key, $value) {
        Arr::set($this->additions, $key, $value);
        return $this;
    }
    
    public function getAddition($key, $default = null) {
        return Arr::get($this->additions, $key, $default);
    }
    
    public function clearAddition() {
        $this->additions = [];
    }
    
    /**
     * @throws InvalidUploadingFileException
     */
    public function validate() {
        $validator = \Validator::make( [
                'title' => $this->title,
                'source_url' => $this->source_url,
                'keywords' => $this->keywords,
                'content' => $this->content,
            ] + $this->additions, [
            'title' => 'required_without_all:link_text,page_title',
            'link_text' => 'required_without_all:title,page_title',
            'page_title' => 'required_without_all:title,link_text',
            'content' => 'required',
        ]);
        if (!$validator->passes()) {
            throw new InvalidUploadingFileException("Not valid uploading file :: " . $validator->errors()->first());
        }
    }
    
    /**
     * @return array[]
     * @throws InvalidUploadingFileException
     */
    public function getMultipart() {
        $this->validate();
        $data = [
            [
                'name' => 'title',
                'contents' => $this->title,
            ],
            [
                'name' => 'source_url',
                'contents' => $this->source_url,
            ],
            [
                'name' => 'keywords',
                'contents' => $this->keywords,
            ],
            [
                'name' => 'single_document',
                'contents' => Utils::streamFor($this->content),
                'filename' => Str::slug( $this->title ) . "tmp",
            ]
        ];
        
        if ($this->additions) {
            foreach ($this->additions as $k => $v) {
                $data[] = [
                    'name' => $k,
                    'contents' => $v,
                ];
            }
        }
        return $data;
    }
    
    public static function fromUrl($url, $title, $source_url = '', $keywords = '', $additions = []): UploadingFile {
        $opts = [
            'http'=> [
                'method'=>"GET",
                'header'=>"User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:64.0) Gecko/20100101 Firefox/64.0\r\n",
            ],
            "ssl"=> [
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ]
        ];
        $context = stream_context_create($opts);
        $content = file_get_contents( $url, false, $context );
        return self::fromContent( $content, $title, $source_url , $keywords, $additions );
    }
    
    public static function fromFile($path, $title, $source_url = '', $keywords = '', $additions = []): UploadingFile {
        $content = file_get_contents( $path );
        return self::fromContent( $content, $title, $source_url , $keywords, $additions );
    }
    
    public static function fromContent($content, $title, $source_url = '', $keywords = '', $additions = []): UploadingFile {
        $instance = new self();
        $instance->content = $content;
        $instance->title = $title;
        $instance->source_url = $source_url;
        $instance->keywords = $keywords;
        $instance->additions = $additions;
        return $instance;
    }
}