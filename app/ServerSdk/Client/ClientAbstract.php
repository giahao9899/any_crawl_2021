<?php


namespace App\ServerSdk\Client;


use App\ServerSdk\Exceptions\ServerBusyException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Arr;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;

abstract class ClientAbstract
{
    protected string $host;
    
    protected string $token;
    
    protected Client $client;
    
    public static array $guzzle_options = [];
    
    /**
     * ClientAbstract constructor.
     *
     * @param string $host
     * @param string $token
     */
    public function __construct(string $host, string $token)
    {
        $this->host = $host;
        $this->token = $token;
        
        $guzzle_config = array_merge(
            [
                'base_uri' => $host,
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                ],
                'verify' => false,
            ],
            static::$guzzle_options,
        );
        $this->client = new Client($guzzle_config);
    }
    
    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }
    
    /**
     * Ping
     * @return bool
     */
    abstract public function ping(): bool;
    
    /**
     * Create and send an HTTP request.
     *
     * Use an absolute path to override the base path of the client, or a
     * relative path to append to the base path of the client. The URL can
     * contain the query string as well.
     *
     * @param string $method HTTP method.
     * @param string|UriInterface $uri URI object or string.
     * @param array $options Request options to apply. See \GuzzleHttp\RequestOptions.
     *
     * @return ResponseInterface
     * @throws ServerBusyException
     * @throws GuzzleException
     */
    public function request(string $method, UriInterface|string $uri = '', array $options = []): ResponseInterface
    {
        try {
            return $this->client->request($method, $uri, $options);
        } catch (GuzzleException $e) {
            if ($e->getCode() === 499) {
                throw new ServerBusyException($e->getMessage());
            } else {
                throw $e;
            }
        }
    }
}