<?php


namespace App\ServerSdk\Client\Web;


use App\ServerSdk\Exceptions\InvalidRequestException;
use App\ServerSdk\Exceptions\ServerBusyException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Utils;

class SeoKeywordClient extends WebClient
{
    const uri_publish_seo_kw = 'sapi/v2/seo_keyword/store';
    
    /**
     * Publish seo_keyword to Web
     *
     * @param string $keyword
     * @param array $parts
     * @param int $documents_count
     * @param array $related_keywords
     * @param array $related_titles
     * @param int $documents_matched
     *
     * @return bool
     * @throws GuzzleException
     * @throws InvalidRequestException
     * @throws ServerBusyException
     */
    public function publish(string $keyword, array $parts, int $documents_count,
                            array $related_keywords = [], array $related_titles = [], int $documents_matched = 0): bool
    {
        $response = $this->request('POST', self::uri_publish_seo_kw, [
            'multipart' => [
                [
                    'name' => 'keyword',
                    'contents' => $keyword,
                ],
                [
                    'name' => 'related_keywords',
                    'contents' => json_encode( $related_keywords ),
                ],
                [
                    'name' => 'related_titles',
                    'contents' => json_encode( $related_titles ),
                ],
                [
                    'name' => 'documents_count',
                    'contents' => $documents_count,
                ],
                [
                    'name' => 'documents_matched',
                    'contents' => $documents_matched,
                ],
                [
                    'name' => 'parts',
                    'contents' => json_encode( $parts ),
                ],
            ]
        ]);
    
        $response = Utils::jsonDecode($response->getBody()->getContents(), true);
        if ($response['success'] == true) {
            return true;
        } else {
            throw new InvalidRequestException("Publish seo_keyword \"$keyword\" error : {$response['message']}");
        }
    }
}