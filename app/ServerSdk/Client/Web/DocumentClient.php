<?php


namespace App\ServerSdk\Client\Web;


use GuzzleHttp\Utils;

class DocumentClient extends WebClient
{
    const uri_sync_info_document = 'sapi/v2/documents/sync_info';
    const uri_sync_meta_document = 'sapi/v2/documents/sync_meta';
    
    /**
     * @param array $data
     *
     * @return array
     * @throws \App\ServerSdk\Exceptions\ServerBusyException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncDocument(array $data) {
        $response = $this->request('POST',  self::uri_sync_info_document, [
            'multipart' => [
                [
                    'name' => 'document',
                    'contents' => json_encode( $data )
                ],
            ]
        ]);
    
        return Utils::jsonDecode($response->getBody()->getContents(), true);
    }
    
    /**
     * @param array $data
     *
     * @return array
     * @throws \App\ServerSdk\Exceptions\ServerBusyException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncMeta(array $data) {
        $response = $this->request('POST', self::uri_sync_meta_document, [
            'multipart' => [
                [
                    'name' => 'documents',
                    'contents' => \GuzzleHttp\json_encode( $data )
                ],
            ]
        ]);
    
        return Utils::jsonDecode($response->getBody()->getContents(), true);
    }
}