<?php


namespace App\ServerSdk\Client\Web;


use GuzzleHttp\Utils;

class FigureClient extends WebClient
{
    const uri_publish_figures = 'sapi/v2/documents/sync_figure';
    
    /**
     * Sync figures to web
     * @param array $data
     *
     * @return array
     * @throws \App\ServerSdk\Exceptions\ServerBusyException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncFigures(array $data) {
        $response = $this->request('POST',  self::uri_publish_figures, [
            'multipart' => [
                [
                    'name' => 'data',
                    'contents' => json_encode($data)
                ],
            ]
        ]);
    
        return Utils::jsonDecode($response->getBody()->getContents(), true);
    }
}