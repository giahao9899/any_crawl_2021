<?php


namespace App\ServerSdk\Client\Web;


use App\ServerSdk\Exceptions\InvalidRequestException;
use App\ServerSdk\Exceptions\ServerBusyException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Utils;

class SeoPartClient extends WebClient
{
    const uri_publish_seo_part = 'sapi/v2/seo_part/store';
    
    /**
     * Publish seo_part to Web
     *
     * @param string $document_code
     * @param array $seo_part
     * @param string $full_text
     *
     * @return bool
     * @throws InvalidRequestException
     * @throws ServerBusyException
     * @throws GuzzleException
     */
    public function publish(string $document_code, array $seo_part, string $full_text): bool
    {
        $response = $this->request('POST', self::uri_publish_seo_part, [
            'multipart' => [
                [
                    'name' => 'document_code',
                    'contents' => $document_code,
                ],
                [
                    'name' => 'heading',
                    'contents' => $seo_part['heading'],
                ],
                [
                    'name' => 'prefix',
                    'contents' => $seo_part['prefix'],
                ],
                [
                    'name' => 'ancestors',
                    'contents' => json_encode(array_values($seo_part['ancestors']) ?? []),
                ],
                [
                    'name' => 'words',
                    'contents' => $seo_part['words'] ?? $seo_part['length'],
                ],
                [
                    'name' => 'heading_quality',
                    'contents' => $seo_part['heading_quality'],
                ],
                [
                    'name' => 'text_quality',
                    'contents' => $seo_part['text_quality'],
                ],
                [
                    'name' => 'start_page',
                    'contents' => $seo_part['start_page'],
                ],
                [
                    'name' => 'end_page',
                    'contents' => $seo_part['end_page'],
                ],
                [
                    'name' => 'start_position',
                    'contents' => json_encode($seo_part['start_position']),
                ],
                [
                    'name' => 'end_position',
                    'contents' => json_encode($seo_part['end_position']),
                ],
                [
                    'name' => 'full_text',
                    'contents' => $full_text,
                ],
            ]
        ]);
    
        $response = Utils::jsonDecode($response->getBody()->getContents(), true);
        if ($response['success'] == true) {
            return true;
        } else {
            throw new InvalidRequestException("Error publish seo_part : " . $response['message']);
        }
    }
}