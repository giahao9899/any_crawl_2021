<?php


namespace App\ServerSdk\Client\Web;


use App\ServerSdk\Exceptions\InvalidRequestException;
use App\ServerSdk\Exceptions\ServerBusyException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Utils;

class SeoTitleClient extends WebClient
{
    const uri_publish_seo_title = 'sapi/v2/seo_title/store';
    
    /**
     * Publish seo_title to web
     *
     * @param string $title
     * @param string|null $code
     * @param array $parts
     * @param int $documents_count
     * @param array $related_titles
     *
     * @return bool
     * @throws GuzzleException
     * @throws InvalidRequestException
     * @throws ServerBusyException
     */
    public function publish(string $title, ?string $code, array $parts, int $documents_count,
                            array $related_titles = []): bool
    {
        $response = $this->request('POST' , self::uri_publish_seo_title, [
            'multipart' => [
                [
                    'name' => 'title',
                    'contents' => $title,
                ],
                [
                    'name' => 'document_code',
                    'contents' => $code,
                ],
                [
                    'name' => 'documents_count',
                    'contents' => $documents_count,
                ],
                [
                    'name' => 'related_titles',
                    'contents' => json_encode($related_titles)
                ],
                [
                    'name' => 'parts',
                    'contents' => json_encode( $parts ),
                ],
            ]
        ]);
    
        $response = Utils::jsonDecode($response->getBody()->getContents(), true);
        if ($response['success'] == true) {
            return true;
        } else {
            throw new InvalidRequestException("Publish seo_title \"$title\" error : {$response['message']}");
        }
    }
}