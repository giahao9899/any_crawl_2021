<?php


namespace App\ServerSdk\Client\Web;


use App\ServerSdk\Client\ClientAbstract;
use App\ServerSdk\Exceptions\ServerBusyException;
use GuzzleHttp\Exception\GuzzleException;

class WebClient extends ClientAbstract
{
    const uri_ping = 'sapi/v2/ping';
    
    public function ping(): bool
    {
        try {
            $response = $this->request('GET', self::uri_ping);
            return $response->getStatusCode() == 200;
        } catch (ServerBusyException|GuzzleException $exception) {
            return false;
        }
    }
}