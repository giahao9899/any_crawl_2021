<?php

namespace App\Services;

use App\Libs\StringUtils;
use App\Models\CrawlUrl;
use App\ServerSdk\Client\Server2Search\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Vuh\CliEcho\CliEcho;

class Web2SearchService
{
    public static function upload(CrawlUrl $crawl_url, Client $client) {
        $document = $crawl_url->data;
        $document['title'] = self::titleNormalize($document['title']);
        $document['content'] = $document['abstract'];

        $document['download_link'] = self::getDownloadLink([...$document['referer_links'], $document['download_link']]);
        $document['referer'] = $crawl_url->url;
        $document = \Arr::only($document, ['title', 'author', 'keywords', 'language', 'download_link', 'referer', 'content']);
        try {
            $upload_result = $client->upload($document);
        } catch (RequestException|GuzzleException $exception) {
            $upload_result = [
                'success' => false,
                'message' => $exception->getMessage(),
            ];
        }

        if($upload_result['success']) {
            $crawl_url->update([
                'upload_status' => CrawlUrl::UPLOAD_DONE,
                'upload_log' => self::getUploadLog($client->getHost(), $upload_result, $crawl_url),
                'uploaded_at' => now(),
            ]);
            CliEcho::infonl( "DONE UPLOAD [$crawl_url->id]");
            dump($upload_result['document']);
        } else {
            $crawl_url->update([
                'upload_status' => CrawlUrl::UPLOAD_FAIL,
                'upload_log' => self::getUploadLog($client->getHost(), $upload_result, $crawl_url),
                'uploaded_at' => now(),
            ]);
            CliEcho::infonl( "FAIL UPLOAD [$crawl_url->id]: {$upload_result['message']}");
        }
    }

    protected static function getUploadLog(string $host, array $result, CrawlUrl $crawl_url) {
        $log = $crawl_url->upload_log;
        if (empty($log[$host])) {
            $log[$host]['remote_id'] = 0;
            $log[$host]['request_payload'] = [];
            $log[$host]['response_payload'] = [];
        }

        if ($result['success']) {
            $log[$host]['remote_id'] = $result['document']['id'];
        } else {
            if (!empty($result['document']) && $result['document']['id']) {
                $log[$host]['remote_id'] = $result['document']['id'];
            } else {
                $log[$host]['response_payload'] = [
                    'message' => $result['message'],
                ];
                $log[$host]['remote_id'] = 0;
            }
        }

        return $log;
    }

    protected static function getDownloadLink(array $urls, $default = null) {
        $client = new \GuzzleHttp\Client();
        foreach ($urls as $url) {
            try {
                $response = $client->head($url);
                if ($response->getStatusCode() == 200) {
                    return $url;
                }
            } catch (GuzzleException) {}
        }

        return $default;
    }

    protected static function titleNormalize(string $title) {
        $title = StringUtils::trim($title);
        $title = preg_replace('/^CiteSeerX — /', '', $title);


        return StringUtils::trim($title);
    }
}