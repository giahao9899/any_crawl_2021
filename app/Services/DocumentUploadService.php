<?php

namespace App\Services;

use App\Libs\TikaHelper;
use App\Models\DownloadLinks;
use App\ServerSdk\Client\DocumentServer\DocumentClient;
use App\ServerSdk\Client\DocumentServer\DocumentFileValidator;
use App\ServerSdk\Client\DocumentServer\Entities\Document;
use App\ServerSdk\Client\DocumentServer\Entities\UploadResult;
use App\ServerSdk\Exceptions\InvalidUploadingFileException;
use Vuh\CliEcho\CliEcho;

class DocumentUploadService
{
    public static function upload(DownloadLinks $download_link, DocumentClient $uploader, DocumentFileValidator $document_validator = null) {
        $opts = [
            'http' => [
                'method' => "GET",
                'header' => "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:64.0) Gecko/20100101 Firefox/64.0\r\n",
            ],
            "ssl" => [
                "verify_peer" => false,
                "verify_peer_name" => false,
            ]
        ];
        $document_content = stream_context_create($opts);
        $document_content = file_get_contents($download_link->url, false, $document_content);

        $additions = [];
        if ($document_validator) {
            try{
                $tika_helper = (new TikaHelper());
            }catch (\Exception $ex){
                dd("Check tika config : " . $ex->getMessage() );
            }
            try{
                $_file = tempnam( sys_get_temp_dir(), 'tika_');
                @file_put_contents( $_file, $document_content );
                $file_info = $tika_helper->getDocumentInfo( $_file );
                $additions['pages'] = $file_info['pages'];
                $additions['content_characters'] = $file_info['characters'];
                $additions['content_words'] = $file_info['words'];
                $additions['original_ext'] = $file_info['extension'];
                $additions['language'] = $file_info['language_code'];
                $additions = $document_validator->check($additions);
                @unlink( $_file );
            }catch (\Exception $ex) {
                CliEcho::infonl( "Parse error: " . $ex->getMessage());
                @unlink( $_file );
            }
        }

        try {
            $upload_result = $uploader->uploadFromContent($document_content, $download_link->data['title'], '', $download_link->url, $additions);
        } catch (InvalidUploadingFileException $exception) {
            $upload_result = new UploadResult( new Document(), false, $exception->getMessage() );
        }

        if($upload_result->isSuccess()) {
            $download_link->update([
                'upload_status' => DownloadLinks::UPLOAD_DONE,
                'upload_log' => self::getUploadLog($uploader->getHost(), $upload_result, $download_link),
                'uploaded_at' => now(),
            ]);
            CliEcho::infonl( "DONE UPLOAD [$download_link->id]");
            dump( $upload_result->getDocument() );
        } else {
            $download_link->update([
                'upload_status' => DownloadLinks::UPLOAD_FAIL,
                'upload_log' => self::getUploadLog($uploader->getHost(), $upload_result, $download_link),
                'uploaded_at' => now(),
            ]);
            CliEcho::infonl( "FAIL UPLOAD [$download_link->id]: {$upload_result->getMessage()}");
        }
    }

    protected static function getUploadLog(string $host, UploadResult $result, DownloadLinks $download_link) {
        $log = $download_link->upload_log;
        if (empty($log[$host])) {
            $log[$host]['remote_id'] = 0;
            $log[$host]['request_payload'] = [];
            $log[$host]['response_payload'] = [];
        }

        if ($result->isSuccess()) {
            $log[$host]['remote_id'] = $result->getDocument()->id;
        } else {
            $log[$host]['response_payload'] = [
                'message' => $result->getMessage(),
            ];
            $log[$host]['remote_id'] = 0;
        }

        return $log;
    }
}
