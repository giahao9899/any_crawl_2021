<?php

namespace App\Libs;


use HocVT\TikaSimple\TikaSimpleClient;
use Symfony\Component\Mime\MimeTypes;

class TikaHelper {

    protected $cache_key = 'tika_server';
    protected $non_space_languages = [
        'ja',
        'th',
        'zh-CN'
    ];
    protected $client;
    protected $guzzle_client;
    protected $mime_helper;

    /**
     * TikaHelper constructor.
     */
    public function __construct() {
        $this->client = new TikaSimpleClient(
            config('tika_helper.host')
        );
        $this->mime_helper = new MimeTypes();
        $this->guzzle_client = new \GuzzleHttp\Client([
            'timeout' => config('tika_helper.timeout')
        ]);
    }

    public function getMime($path){
        $mime_info = $this->client->mimeFile( $path );
        return $mime_info;
    }

    public function getExtension($path){
        $mime_info = $this->getMime( $path );
        $ext = $this->mime_helper->getExtensions( $mime_info )[0];
        return $ext;
    }

    public function getLanguage(string $string) : string {
        return $this->client->language($string);
    }

    public function getDocumentInfo($path){
        $meta_data = $this->client->rmetaFile( $path, 'text' );

        $data = [
            'title' => $meta_data['pdf:docinfo:title'] ?? $meta_data['dc:title'] ?? $meta_data['title'] ?? '',
            'pages' => $meta_data['meta:page-count'] ?? $meta_data['Page-Count'] ?? $meta_data['xmpTPg:NPages'] ?? 0,
            'words' => $meta_data['meta:word-count'] ?? 0,
            'size' => filesize( $path ),
            'characters' => mb_strlen($meta_data['X-TIKA:content']),
            'mime_type' => $meta_data['Content-Type'],
            'extension' => $this->mime_helper->getExtensions( $meta_data['Content-Type'] )[0],
            'content' => str_replace( "\n\n", "\n", $meta_data['X-TIKA:content'] ),
        ];

        list( $data['characters'], $data['words'], $data['description'], $data['language_code'])
            = $this->parse_content( $meta_data );
        return $data;
    }

    protected function parse_content($meta_data){

        $content = trim( $meta_data['X-TIKA:content']);

        $content = preg_replace( "/[\n\t\r\s\b\.\-\_…·\:]+/ui", " ", $content);

        $content = trim( $content );
        $characters = mb_strlen( $content );

        $language = $this->getLanguage( \Illuminate\Support\Str::limit( $content, 1000, '' ) );
        $description = \Illuminate\Support\Str::limit( $content, 200, '[.]' );

        if(in_array( $language, $this->non_space_languages)){
            $words = mb_strlen( str_replace( " ", "", $content));
        }else{
            $words = count( explode( " ", $content));
        }

        return [$characters, $words, $description, $language];
    }

}
