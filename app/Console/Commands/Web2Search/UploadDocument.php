<?php

namespace App\Console\Commands\Web2Search;

use App\Crawler\Enum\DataStatus;
use App\Models\CrawlUrl;
use App\ServerSdk\Client\Server2Search\Client;
use App\Services\Web2SearchService;
use Illuminate\Console\Command;

class UploadDocument extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'web2search:upload
    {--id= : id}
    {--language=en : Language}
    {--host= : Host to upload}
    {--token= : Token to access host, Ex: app_name:app_token}
    {--pre-parse : Use tika parse and check valid before upload}
    {--delay=0 : Delay (s)}
    {--limit=0 : limit document limit}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload document to 2search.net';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $language = $this->option('language');
        $host = $this->option('host');
        $token = $this->option('token') ?: '';
        $pre_parse = $this->option('pre-parse');
        $delay = $this->option('delay');
        $limit = $this->option('limit');
        $id = $this->option('id');

        $this->info('Uploading to ' . $host);

        $uploader = new Client($host, $token);

        if ($id) {
            $crawl_url = CrawlUrl::where('id', $id)
                                ->where('data_status', DataStatus::HAS_DATA)
                                ->first();
            Web2SearchService::upload($crawl_url, $uploader);
        } elseif ($limit > 0) {
            $uploaded_count = 0;
            CrawlUrl::where('data_status', DataStatus::HAS_DATA)
                ->where('upload_status', CrawlUrl::UPLOAD_INIT)
                ->chunkById(50, function ($crawl_urls)
                use ($uploader, $delay, $limit, &$uploaded_count) {
                    /** @var CrawlUrl $crawl_url */
                    foreach ($crawl_urls as $crawl_url) {
                        Web2SearchService::upload($crawl_url, $uploader);
                        if ($limit && ++$uploaded_count >= $limit ) dd("DONE UPLOAD $uploaded_count DOCUMENTS");
                        sleep($delay);
                    }
                });
            $this->info("DONE UPLOAD $uploaded_count DOCUMENTS");

        } else {
            while (true) {
                $crawl_urls = CrawlUrl::where('data_status', DataStatus::HAS_DATA)
                    ->where('upload_status', CrawlUrl::UPLOAD_INIT)
                    ->take(20)
                    ->get();
                foreach ($crawl_urls as $crawl_url) {
                    Web2SearchService::upload($crawl_url, $uploader);
                    sleep($delay);
                }
                if ($crawl_urls->count() < 1) {
                    $this->info('Pending......');
                    sleep(60);
                }
            }

        }
        return self::SUCCESS;
    }
}
