<?php

namespace App\Console\Commands;

use App\Models\DownloadLinks;
use App\ServerSdk\Client\DocumentServer\DocumentClient;
use App\ServerSdk\Client\DocumentServer\DocumentFileValidator;
use App\Services\DocumentUploadService;
use Illuminate\Console\Command;

class UploadDocument extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:auto
    {--id= : id}
    {--language=en : Language}
    {--host= : Host to upload}
    {--token= : Token to access host, Ex: app_name:app_token}
    {--pre-parse : Use tika parse and check valid before upload}
    {--delay=0 : Delay (s)}
    {--limit=0 : limit document limit}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        $language = $this->option('language');
        $host = $this->option('host');
        $token = $this->option('token');
        $pre_parse = $this->option('pre-parse');
        $delay = $this->option('delay');
        $limit = $this->option('limit');
        $id = $this->option('id');

        if (empty($host) | empty($token)) {
            $upload_config = config('upload.'.$language);
            $host = $upload_config['host'];
            $token = $upload_config['token'];
        }

        $this->info('Uploading to ' . $host);

        $uploader = new DocumentClient($host, $token);

        if ($pre_parse) {
            $document_validator = DocumentFileValidator::createFromHost($host);
        } else {
            $document_validator = null;
        }

        if ($id) {
            $download_link = DownloadLinks::where('id', $id)->first();
            DocumentUploadService::upload($download_link, $uploader, $document_validator);
        } elseif ($limit > 0) {
            $uploaded_count = 0;
            DownloadLinks::where('language', $language)
                ->where('upload_status', DownloadLinks::UPLOAD_INIT)
                ->chunkById(50, function ($download_links)
                use ($uploader, $document_validator, $delay, $limit, &$uploaded_count) {
                    /** @var DownloadLinks $download_link */
                    foreach ($download_links as $download_link) {
                        DocumentUploadService::upload($download_link, $uploader, $document_validator);
                        if ($limit && ++$uploaded_count >= $limit ) dd("DONE UPLOAD $uploaded_count DOCUMENTS");
                        sleep($delay);
                    }
                });
            $this->info("DONE UPLOAD $uploaded_count DOCUMENTS");

        } else {
            while (true) {
                $document_links = DownloadLinks::where('language', $language)
                    ->where('upload_status', DownloadLinks::UPLOAD_INIT)
                    ->take(20)
                    ->get();
                foreach ($document_links as $document_link) {
                    DocumentUploadService::upload($document_link, $uploader, $document_validator);
                    sleep($delay);
                }
                if ($document_links->count() < 1) {
                    $this->info('Pending......');
                    sleep(60);
                }
            }

        }


        return self::SUCCESS;
    }
}
