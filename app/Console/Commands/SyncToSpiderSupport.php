<?php

namespace App\Console\Commands;

use App\Models\Referrer;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class SyncToSpiderSupport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spider_support:sync
    {--host= : host}
    {--token= : token}
    {--id= : id, id range}
    ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $host = $this->option('host');
        $token = $this->option('token');
        $id = $this->option('id');

        $client = new Client([
            'base_uri' => $host,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ],
            'verify' => false,
        ]);

        Referrer::when($id, function (Builder $query) use ($id) {
            if (strpos($id, '-')) {
                [$min, $max] = explode("-", $id);
                $query->where('id', ">=", $min);
                $query->where('id', "<=", $max);
            } elseif (strpos($id, ',')) {
                $ids = explode(',', $id);
                $query->whereIn('id', $ids);
            } else {
                $query->where('id', $id);
            }
        })
        ->chunkById(10, function ($referrers) use ($client) {
            /** @var Referrer $referrer */
            foreach ($referrers as $referrer) {
                $this->info("Sync $referrer->start_url");

                try {
                    $client->post('api/site/sync', [
                        'form_params' => [
                            'site_info' => $referrer->toArray(),
                        ],
                    ]);
                } catch (\Exception|GuzzleException $exception) {
                    $this->error($exception->getMessage());
                }
                sleep(1);
            }
        });
        return Command::SUCCESS;
    }
}
