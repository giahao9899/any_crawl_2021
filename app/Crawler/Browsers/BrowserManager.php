<?php

namespace App\Crawler\Browsers;

use App\Crawler\Exceptions\NotFoundBrowserDriver;

final class BrowserManager
{
    protected static array $drivers = [];

    /**
     * @param string $driver
     *
     * @return BrowserInterface
     * @throws NotFoundBrowserDriver
     */
    public static function get(string $driver = "guzzle"): BrowserInterface
    {
        if (empty(self::$drivers[$driver])) {
            self::$drivers[$driver] = self::makeBrowser($driver);
        }
        return self::$drivers[$driver];
    }

    /**
     * @param string $driver
     *
     * @return BrowserInterface
     * @throws NotFoundBrowserDriver
     */
    protected static function makeBrowser(string $driver = "guzzle"): BrowserInterface
    {
        return match ($driver) {
            "puppeteer" => new Puppeteer(),
            "guzzle" => new Guzzle(),
            "browserless" => new Browserless(),
            default => throw new NotFoundBrowserDriver("No browser match with driver " . $driver),
        };
    }
}
