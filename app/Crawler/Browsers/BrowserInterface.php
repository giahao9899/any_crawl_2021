<?php

namespace App\Crawler\Browsers;

interface BrowserInterface
{
    /**
     * Get html
     * @param string $url
     *
     * @return mixed
     */
    public function getHtml(string $url);

}
