<?php

namespace App\Crawler\Sites;

use GuzzleHttp\Psr7\Uri;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;

abstract class SiteAbstract implements SiteInterface
{
    public function __construct(
        public string $root_url
    ){}

    public function rootUrl(): string
    {
        return $this->root_url;
    }

    public function maxDepth(): int
    {
        return -1;
    }

    public function delay(): int
    {
        return 0;
    }

    public function shouldCrawl(string $url)
    {
        return true;
    }

    public function shouldGetData(string $url)
    {
        return true;
    }

    public function normalizeUrl(string|Uri $url)
    {
        return $url;
    }

    public function getInfoFromCrawler(DomCrawler $dom_crawler, string $url = '')
    {
        $title = $dom_crawler->filterXPath('//title')->text();

        return compact('title');
    }

    public function __toString(): string
    {
        return $this->root_url;
    }


}