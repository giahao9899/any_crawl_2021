<?php

namespace App\Crawler\Sites\Resources;

use App\Crawler\CrawlUrl;
use App\Crawler\Sites\SiteAbstract;
use App\Libs\PhpUri;
use App\Libs\StringUtils;
use App\Models\DownloadLinks;
use App\Models\Referrer;
use GuzzleHttp\Psr7\Uri;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;

class Citeseerx extends SiteAbstract
{

    public function startUrls(): array
    {
        return [
            'https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.675.6863',
            'https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.463.8627'
        ];
    }

    public function shouldCrawl(string $url)
    {
        return preg_match("/^https:\/\/citeseerx\.ist\.psu\.edu\/(search|viewdoc\/summary)/", $url);
    }

    public function shouldGetData(string $url)
    {
        return preg_match("/^https:\/\/citeseerx\.ist\.psu\.edu\/viewdoc\/summary/", $url);
    }

    public function normalizeUrl(Uri|string $url)
    {
        $url = preg_replace("/;jsessionid=[0-9a-z]*/ui", '', $url);
        if (preg_match("/^https:\/\/citeseerx\.ist\.psu\.edu\/search/", $url)) {
            $url = preg_replace("/&submit=Search&sort=rlv&t=doc/ui", '', $url);
            $url = preg_replace("/&t=table&sort=rlv/ui", '', $url);
            $url = preg_replace("/&t=auth&uauth=1&sort=ndocs/ui", '', $url);
            $url = preg_replace("/&t=doc&sort=rlv/ui", '', $url);
        } elseif (preg_match("/^https:\/\/citeseerx\.ist\.psu\.edu\/viewdoc\/summary/", $url)) {
            $url = preg_replace('/(https:\/\/citeseerx.ist.psu.edu\/viewdoc\/summary\?doi=[.0-9]+)(&.*)/ui', '$1', $url);
        }
        return parent::normalizeUrl($url);
    }

    public function getInfoFromCrawler(DomCrawler $dom_crawler, string $url = '')
    {
        $title = $dom_crawler->filterXPath('//title')->text();
        $author = $dom_crawler->filterXPath('//*[@id="docAuthors"]')->text();
        $keywords = $dom_crawler->filterXPath('//*[@id="keywords"]/p/a')->each(function (DomCrawler $node) {
            return $node->text();
        });
        $abstract = $dom_crawler->filterXPath('//*[@id="abstract"]/p')->text();
        try {
            $language = StringUtils::detectLanguageByTika($abstract);
        } catch (\Exception) {
            $language = StringUtils::detectLanguage($abstract);
        }

        $download_link = $this->extractDownloadLinks($dom_crawler, $url, $language, $title);
        $referer_links = $this->extractReferer($dom_crawler);

        return compact('title', 'author', 'language', 'keywords', 'abstract', 'download_link', 'referer_links');
    }

    protected function extractDownloadLinks(DomCrawler $dom_crawler, string $url, string $language, string $title) {
        $download_link = $dom_crawler->filterXPath('//*[@id="clinks"]/li/a')->attr('href');
        $download_link = PhpUri::parse($this->rootUrl())->join($download_link);
        $download_link = $this->normalizeUrl($download_link);

        if ($download_link) {
            DownloadLinks::firstOrCreate([
                'url_hash' => CrawlUrl::hashUrl($download_link),
            ], [
                'page_hash' => !empty($url) ? CrawlUrl::hashUrl($url) : null,
                'site' => $this->rootUrl(),
                'url' => $download_link,
                'language' => $language,
                'data' => ['title' => $title],
            ]);
        }

        return $download_link;
    }

    protected function extractReferer(DomCrawler $dom_crawler) {
        $referer_links = $dom_crawler->filterXPath('//*[@id="dlinks"]/li/a')->each(function (DomCrawler $node) {
            return $node->attr('href');
        });

        if (empty($referer_links)) return $referer_links;

        foreach ($referer_links as $link) {
            $uri = PhpUri::parse($link);
            if (!in_array($uri->scheme, ['http', 'https'])) continue;

            $start_url = $uri->getHomeUrl();
            $referer = Referrer::firstOrCreate([
                'start_url_hash' => CrawlUrl::hashUrl($start_url),
            ], [
                'start_url' => $start_url,
            ]);
            $urls = [$link];
            if (!empty($referer->urls)) {
                $urls = array_values(array_unique(array_merge($referer->urls, $urls)));
            }
            $referer->urls = array_slice($urls, 0, 30);
            $referer->save();
        }

        return $referer_links;
    }
}
