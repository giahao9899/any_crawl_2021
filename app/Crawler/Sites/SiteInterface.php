<?php

namespace App\Crawler\Sites;

use GuzzleHttp\Psr7\Uri;
use Symfony\Component\DomCrawler\Crawler;

interface SiteInterface
{
    public function rootUrl(): string;

    public function startUrls(): array;

    public function maxDepth(): int;

    public function delay(): int;

    public function shouldCrawl(string $url);

    public function shouldGetData(string $url);

    public function normalizeUrl(string|Uri $url);

    public function getInfoFromCrawler(Crawler $dom_crawler, string $url = '');

    public function __toString(): string;
}