<?php

namespace App\Crawler\Sites;

use App\Crawler\Exceptions\SiteNotFoundException;
use App\Crawler\Sites\Resources\Citeseerx;

final class SiteManager
{
    protected static array $sites = [
        'https://citeseerx.ist.psu.edu' => Citeseerx::class,
    ];

    /**
     * @throws SiteNotFoundException
     */
    public static function getSiteConfig(string $name) {
        if (empty(self::$sites[$name])) {
            throw new SiteNotFoundException("Can not find site match with name : " . $name);
        }

        return new self::$sites[$name]($name);
    }
}