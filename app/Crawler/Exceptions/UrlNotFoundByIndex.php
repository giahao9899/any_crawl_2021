<?php

namespace App\Crawler\Exceptions;

use RuntimeException;

class UrlNotFoundByIndex extends RuntimeException
{
}