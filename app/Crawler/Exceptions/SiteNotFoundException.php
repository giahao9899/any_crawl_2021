<?php

namespace App\Crawler\Exceptions;

class SiteNotFoundException extends \Exception
{
}