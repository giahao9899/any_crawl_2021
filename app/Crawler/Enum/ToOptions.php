<?php

namespace App\Crawler\Enum;

trait ToOptions
{

    /**
     * @return array
     * @throws \ReflectionException
     */
    public static function toOptions()
    {
        $class = \get_called_class();

        if (!isset(static::$cache[$class])) {
            $reflection            = new \ReflectionClass($class);
            static::$cache[$class] = $reflection->getConstants();
        }

        $array = static::$cache[$class];
        return array_flip( $array );
    }
}