<?php

namespace App\Crawler\Enum;

use MyCLabs\Enum\Enum;

class DataStatus extends Enum
{
    use ToOptions;

    public const INIT = 0;
    public const HAS_DATA = 1;
    public const NO_DATA = -1;
    public const GET_DATA_ERROR = -2;
}