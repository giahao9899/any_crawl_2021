<?php

namespace App\Crawler\Enum;


use MyCLabs\Enum\Enum;

class CrawlStatus extends Enum {

    use ToOptions;

    public const INIT = 0;
    public const VISITING = 10;
    public const DONE = 200; // default success code
    public const FAIL = 1000; // default error code, or response code for specific error

}