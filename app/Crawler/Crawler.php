<?php

namespace App\Crawler;

use App\Crawler\Browsers\BrowserManager;
use App\Crawler\Enum\CrawlStatus;
use App\Crawler\Queue\QueueInterface;
use App\Crawler\Sites\SiteInterface;
use App\Crawler\Sites\SiteManager;
use App\Libs\PhpUri;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;
use GuzzleHttp\Psr7\Uri;
use Vuh\CliEcho\CliEcho;

class Crawler
{
    public function __construct(
        protected QueueInterface $queue
    ) {}

    /**
     * @throws Exceptions\SiteNotFoundException
     * @throws Exceptions\NotFoundBrowserDriver
     */
    public function run(string $site, bool $reset = false)
    {
        $site = SiteManager::getSiteConfig($site);
        $this->init($site, $reset);
        while ($this->queue->hasPendingUrls($site)) {
            $crawl_url = $this->queue->firstPendingUrl($site);
            if (empty($crawl_url)) continue;
            try {
                CliEcho::infonl("Goto: [$crawl_url->url]");
                $html = BrowserManager::get()->getHtml($crawl_url->url);
            } catch (GuzzleException $exception) {
                CliEcho::errornl($exception->getMessage());
                if (in_array($exception->getCode(), config('crawler.should_retry_status_codes'))) {
                    $crawl_url->setStatus(CrawlStatus::INIT);
                } else {
                    $crawl_url->setStatus(CrawlStatus::FAIL);
                }
                continue;
            }

            try {
                // Symfony/crawler
                $dom_crawler = new DomCrawler($html);

                $urls = $this->extractLinks($site, $dom_crawler);
                if($site->shouldGetData($crawl_url->url)) {
                    $data = $site->getInfoFromCrawler($dom_crawler, $crawl_url->url);
                    dump($data);
                    $crawl_url->setData($data);
                }
                $crawl_url->setStatus(CrawlStatus::DONE);
                $this->queue->changeProcessStatus($crawl_url, $crawl_url->getStatus());
                foreach ($urls as $url) {
                    if (!$site->shouldCrawl($url)) {
                        continue;
                    }
                    $crawl_url = CrawlUrl::create($site, new Uri($url), $crawl_url->url);
                    $this->queue->push($crawl_url);
                }
            } catch (\Exception $exception) {
                CliEcho::errornl($exception->getMessage());
                $crawl_url->setStatus(CrawlStatus::FAIL);
            }
        }
    }

    protected function extractLinks(SiteInterface $site, DomCrawler $dom_crawler) {
        $urls_selector = $dom_crawler->filterXpath('//a | //link[@rel="next" or @rel="prev"]');
        $urls = [];
        /** @var \DOMElement $item */
        foreach ($urls_selector as $item) {
            $item = $item->getAttribute('href');
            $item = PhpUri::parse($site)->join($item);
            if ($site->shouldCrawl($item)) {
                $urls[] = $site->normalizeUrl($item);
            }
        }
        return array_unique($urls);
    }

    protected function init(SiteInterface $site, bool $reset = false) {
        if ($reset) {
            $this->queue->reset($site);
        }
        foreach ($site->startUrls() as $url) {
            $crawl_url = CrawlUrl::create($site, new Uri($url));
            if ($this->queue->push($crawl_url)) {
                CliEcho::successnl("[$site] Added $crawl_url->url");
            }
        }
    }
}
