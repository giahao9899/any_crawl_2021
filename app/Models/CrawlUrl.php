<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class CrawlUrl extends Model
{
    use CrudTrait;

    const UPLOAD_INIT = 0;
    const UPLOAD_FAIL = -1;
    const UPLOAD_DONE = 1;

    protected $table = 'crawl_urls';
    protected $guarded = ['id'];

    protected $casts = [
        'data' => 'array',
    ];
}
