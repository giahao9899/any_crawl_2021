<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Referrer extends Model
{
    use CrudTrait;

    protected $table = 'referrers';
    protected $guarded = ['id'];

    protected $casts = [
        'urls' => 'array',
    ];
}
