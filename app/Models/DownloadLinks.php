<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class DownloadLinks extends Model
{
    use CrudTrait;

    const UPLOAD_INIT = 0;
    const UPLOAD_FAIL = -1;
    const UPLOAD_DONE = 1;

    protected $table = 'download_links';
    protected $guarded = ['id'];
    protected $casts = [
        'data' => 'array',
        'upload_log' => 'array',
        'uploaded_at' => 'datetime'
    ];
}
