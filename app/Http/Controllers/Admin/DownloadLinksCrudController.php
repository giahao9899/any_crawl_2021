<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DownloadLinksRequest;
use App\Libs\TextReducer;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class DownloadLinksCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DownloadLinksCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\DownloadLinks::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/download-links');
        CRUD::setEntityNameStrings('download links', 'download links');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumns([
            ['name' => 'id'],
            ['name' => 'site'],
            [
                'name' => 'url',
                'type'     => 'closure',
                'function' => function($entry) {
                    return "<a href='$entry->url' target='_blank'>". TextReducer::url($entry->url, 50) ."</a>";
                }
            ],
            [
                'name' => 'data',
                'type' => 'json',
                'escaped' => true
            ]
        ]);
    }
}
