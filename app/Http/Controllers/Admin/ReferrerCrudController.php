<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReferrerRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ReferrerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReferrerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Referrer::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/referrer');
        CRUD::setEntityNameStrings('referrer', 'referrers');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumns([
            ['name' => 'id'],
            [
                'name' => 'start_url',
                'label' => 'Start Url',
                'type'     => 'closure',
                'function' => function($entry) {
                    return "<a href='$entry->start_url' target='_blank'>$entry->start_url</a>
                            <br>
                            <a href='https://google.com/search?q=site:$entry->start_url filetype:pdf' target='_blank'><i class='la la-search'></i></a>";
                }
            ],
            [
                'name' => 'urls',
                'type' => 'json'
            ]
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }
}
