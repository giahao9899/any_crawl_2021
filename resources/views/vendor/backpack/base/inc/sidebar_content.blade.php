<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('crawl-url') }}'><i class='nav-icon la la-question'></i> Crawl urls</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('download-links') }}'><i class='nav-icon la la-question'></i> Download links</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('referrer') }}'><i class='nav-icon la la-question'></i> Referrers</a></li>