<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUploadIntoCrawlUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crawl_urls', function (Blueprint $table) {
            $table->tinyInteger('upload_status')->index()->default(0)->comment('0/1/-1');
            $table->dateTime('uploaded_at')->nullable();
            $table->json('upload_log')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crawl_urls', function (Blueprint $table) {
            $table->dropColumn(['upload_status', 'uploaded_at', 'upload_log']);
        });
    }
}
