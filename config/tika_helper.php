<?php
return [
    'host' => env('TIKA_HOST', 'http://localhost:9998'),

    'timeout' => env('TIKA_TIMEOUT', null),
];
